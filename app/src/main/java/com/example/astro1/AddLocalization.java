package com.example.astro1;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class AddLocalization extends AppCompatActivity {

    Button addLocalization;
    EditText editLocalization;
    LinearLayout buttonList;
    String[] buttons = new String[100];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_localization);

        buttonList = findViewById(R.id.buttonList);
        addLocalization = findViewById(R.id.addLocalization);
        editLocalization = findViewById(R.id.editLocalization);
        addLocalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Button button = new Button(getApplicationContext());
                button.setText(editLocalization.getText());
                button.setBackgroundColor(Color.parseColor("#AAAAAA"));
                button.setPadding(0,10,0,10);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(AddLocalization.this,ForecastActivity.class);
                        intent.putExtra("localizationInfo",button.getText().toString().replaceAll("\\s+",""));
                        startActivity(intent);
                    }
                });
                buttonList.addView(button);
            }
        });

    }

}
