package com.example.astro1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.astro1.Common.Common;
import com.example.astro1.Model.WeatherResult;
import com.example.astro1.Retrofit.IOpenWeatherMap;
import com.example.astro1.Retrofit.RetrofitClient;
import com.squareup.picasso.Picasso;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;


/**
 * A simple {@link Fragment} subclass.
 */
public class TodayFragment extends Fragment {


    String userInput;
    TextView txt_city_name;
    ImageView img_weather;
    TextView txt_city;
    TextView txt_pressure;
    TextView txt_humidity;
    TextView txt_sunrise;
    TextView txt_sunset;
    TextView txt_temperature;
    TextView txt_date_time;
    TextView txt_wind;
    TextView txt_geo_coords;

    LinearLayout weather_panel;

    CompositeDisposable compositeDisposable;
    IOpenWeatherMap nService;

    public TodayFragment() {
        // Required empty public constructor
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetrofitClient.getInstance();
        nService = retrofit.create(IOpenWeatherMap.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today, container, false);
        txt_city_name = view.findViewById(R.id.txt_city_name);
        img_weather = view.findViewById(R.id.img_weather);
        txt_city = view.findViewById(R.id.txt_city_name);
        txt_date_time = view.findViewById(R.id.txt_date_time);
        txt_geo_coords = view.findViewById(R.id.txt_coords);
        txt_humidity = view.findViewById(R.id.txt_humidity);
        txt_pressure = view.findViewById(R.id.txt_pressure);

        txt_sunrise = view.findViewById(R.id.txt_sunrise);
        txt_sunset = view.findViewById(R.id.txt_sunset);
        txt_temperature = view.findViewById(R.id.txt_temperature);
        txt_wind = view.findViewById(R.id.txt_wind);
        weather_panel = view.findViewById(R.id.weather_panel);


        ForecastActivity activity = (ForecastActivity) getActivity();
        userInput = activity.userInput;
        getWeatherInfo();


        return view;
    }

    private void getWeatherInfo() {
        if(userInput!=null)
            compositeDisposable.add(nService.getWeatherByCityName(userInput,
                    Common.APP_ID,
                    "metric")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<WeatherResult>() {
                        @Override
                        public void accept(WeatherResult weatherResult) throws Exception {

                            Picasso.get().load(new StringBuilder("https://openweathermap.org/img/w/")
                                    .append(weatherResult.getWeather().get(0).getIcon())
                                    .append(".png").toString())
                                    .resize(300, 300)
                                    .into(img_weather);


                            txt_city.setText("Weather in "+weatherResult.getName());

                            txt_date_time.setText(Common.convertUnixToDate(weatherResult.getDt()));
                            txt_temperature.setText(new StringBuilder(
                                    String.valueOf(weatherResult.getMain().getTemp()))
                                    .append(" °C")
                                    .toString());
                            txt_wind.setText(new StringBuilder("Speed:")
                                    .append(String.valueOf(weatherResult.getWind().getSpeed()))
                                    .append(" Deg: ")
                                    .append(String.valueOf(weatherResult.getWind().getDeg())));

                            txt_pressure.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getPressure()))
                                    .append(" hpa")
                                    .toString());

                            txt_humidity.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getHumidity()))
                                    .append(" %")
                                    .toString());

                            txt_sunrise.setText(Common.convertUnixToHour(weatherResult.getSys().getSunrise()));

                            txt_sunset.setText(Common.convertUnixToHour(weatherResult.getSys().getSunset()));
                            txt_geo_coords.setText(new StringBuilder(weatherResult.getCoord().toString())
                                    .toString());


                            weather_panel.setVisibility(View.VISIBLE);

                        }

                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }));

    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

}
