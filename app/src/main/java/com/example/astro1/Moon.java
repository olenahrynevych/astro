package com.example.astro1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

import java.util.Calendar;
import java.util.Date;

public class Moon extends Fragment {


    TextView moonFull;
    TextView moonNew;
    TextView moonRise;
    TextView moonSet;
    TextView moonPhase;
    TextView moonIllumination;

    Date currentTime;
    AstroDateTime dateTime;
    View view;


    AstroCalculator.Location location;
    AstroCalculator astroCalculator;
    AstroCalculator.MoonInfo info;
    AstroDateTime moonSetTime;
    AstroDateTime moonRiseTime;
    AstroDateTime moonFullTime;
    AstroDateTime moonNewTime;
    double phase;
    double illumination;

    double latitude, longtitude;


    public Moon() {
        this.currentTime = Calendar.getInstance().getTime();
        getMoonParams(this.currentTime);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_moon, container, false);
        moonRise = view.findViewById(R.id.moonRise);
        moonSet = view.findViewById(R.id.moonSet);
        moonFull = view.findViewById(R.id.moonFull);
        moonNew = view.findViewById(R.id.moonNew);
        moonPhase = view.findViewById(R.id.moonPhase);
        moonIllumination = view.findViewById(R.id.moonIllumination);
        setMoonParams();
        return view;
    }

    private void setMoonParams() {
        if (moonRise!=null){
            moonRise.setText(getDateAndTime(moonRiseTime));
            moonSet.setText(getDateAndTime(moonSetTime));
            moonFull.setText(getDateAndTime(moonFullTime));
            moonNew.setText(getDateAndTime(moonNewTime));
            moonPhase.setText(String.valueOf(phase));
            moonIllumination.setText(String.valueOf(illumination));
        }
    }

    private void getMoonParams(Date newTime) {
        dateTime = new AstroDateTime(
                newTime.getYear(),
                newTime.getMonth(),
                newTime.getDay(),
                newTime.getHours(),
                newTime.getMinutes(),
                newTime.getSeconds(),
                newTime.getTimezoneOffset(),
                true);


        //51.759445, 19.457216
        location = new AstroCalculator.Location(this.latitude, this.longtitude);
        astroCalculator = new AstroCalculator(dateTime, location);

        info = astroCalculator.getMoonInfo();
        moonRiseTime = info.getMoonrise();
        moonFullTime = info.getNextFullMoon();
        moonNewTime = info.getNextNewMoon();
        moonSetTime = info.getMoonset();
       // phase = info.getAge();
        phase += 2;
        illumination = info.getIllumination();
    }

    public void updateTime(Date time, double latitude, double longtitude) {
        System.out.println("Moon time updating...");
        this.currentTime = time;
        this.latitude = latitude;
        this.longtitude = longtitude;

        System.out.println("currentTime: " + this.currentTime + ", latitude: " + this.latitude + " longtitude: " + this.longtitude);
        getMoonParams(time);
        setMoonParams();

    }

    public String getDateAndTime(AstroDateTime moonRiseTime) {
        int day = moonRiseTime.getDay();
        int month = moonRiseTime.getMonth();
        int year = moonRiseTime.getYear();


        String d;
        if (day < 10) {
            d = "0" + day;
        } else {
            d = day + "";
        }

        String mn;
        if (day < 10) {
            mn = "0" + month;
        } else {
            mn = month + "";
        }

        int hour = moonRiseTime.getHour();
        int min = moonRiseTime.getMinute();
        int sec = moonRiseTime.getSecond();
        String h;
        if (hour < 10) {
            h = "0" + hour;
        } else {
            h = hour + "";
        }

        String m;
        if (min < 10) {
            m = "0" + min;
        } else {
            m = min + "";
        }

        String s;

        if (sec < 10) {
            s = "0" + sec;
        } else {
            s = sec + "";
        }

        String time = " " + h + ":" + m + ":" + s;
        String date = d + "/" + mn + "/" + year;


        return date + time;

    }

}
