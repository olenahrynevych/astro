package com.example.astro1.Model;

public class Coord {
    private double lon ;
    private double lat ;

    public Coord() {
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    @Override
    public String toString() {
        return "["+getLat()+" , "+getLon() +"]";
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
