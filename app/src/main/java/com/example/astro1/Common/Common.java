package com.example.astro1.Common;

import android.location.Location;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Common {
    public static final String APP_ID = "af498ddca9a4c7a796ae2cdfdb07514f";
    public static Location current_location = null;

    public static String convertUnixToDate(long dt) {
        Date date = new Date(dt * 1000L);
        SimpleDateFormat sfd = new SimpleDateFormat("EEE, MMM dd");
        String formatted = sfd.format(date);
        return formatted;
    }

    public static String convertUnixToHour(long sunrise) {

        Date date = new Date(sunrise * 1000L);
        SimpleDateFormat sfd = new SimpleDateFormat("HH:mm");
        String formatted = sfd.format(date);
        return formatted;

    }
}
