package com.example.astro1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class Sun extends Fragment {


    TextView sunRise;
    TextView sunRiseAzimut;
    TextView sunSet;
    TextView sunSetAzimut;
    TextView sunTwMor;
    TextView sunTwEven;

    Date currentTime;

    AstroDateTime dateTime;
    AstroCalculator.Location location;
    AstroCalculator astroCalculator;


    AstroCalculator.SunInfo infoSun;
    AstroDateTime sunRiseTime;
    AstroDateTime sunSetTime;
    double sunRiseAzim;
    double sunSetAzim;

    AstroDateTime twilightEvening;
    AstroDateTime twilightMorning;
    private double latitude;
    private double longtitude;

    public Sun() {
        currentTime = Calendar.getInstance().getTime();
        getSunParams(currentTime);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sun, container, false);
        sunRise = view.findViewById(R.id.sunRise);
        sunRiseAzimut = view.findViewById(R.id.sunRiseAzimut);
        sunSet = view.findViewById(R.id.sunSet);
        sunSetAzimut = view.findViewById(R.id.sunSetAzim);
        sunTwMor = view.findViewById(R.id.sunTwMor);
        sunTwEven = view.findViewById(R.id.sunTwEven);
        setSunParams();
        return view;

    }

    private void getSunParams(Date newTime) {
        location = new AstroCalculator.Location(latitude, longtitude);
        dateTime = new AstroDateTime(
                newTime.getYear(),
                newTime.getMonth(),
                newTime.getDay(),
                newTime.getHours(),
                newTime.getMinutes(),
                newTime.getSeconds(),
                newTime.getTimezoneOffset(),
                true);
        astroCalculator = new AstroCalculator(dateTime, location);
        infoSun = astroCalculator.getSunInfo();
        sunRiseTime = infoSun.getSunrise();
        sunSetTime = infoSun.getSunset();
        sunRiseAzim = infoSun.getAzimuthRise();
        sunSetAzim = infoSun.getAzimuthSet();
        twilightEvening = infoSun.getTwilightEvening();
        twilightMorning = infoSun.getTwilightMorning();

    }

    private void setSunParams() {
        if(sunRise!=null) {
            sunRise.setText(sunRiseTime.toString());
            sunRiseAzimut.setText(String.valueOf(sunRiseAzim));
            sunSet.setText(sunSetTime.toString());
            sunSetAzimut.setText(String.valueOf(sunSetAzim));
            sunTwMor.setText(twilightMorning.toString());
            sunTwEven.setText(twilightEvening.toString());
        }
    }

    public void updateTime(Date time, double latitude, double longtitude) {
        System.out.println("Sun time updating...");
        this.currentTime = time;
        this.latitude = latitude;
        this.longtitude = longtitude;

        System.out.println("currentTime: " + this.currentTime + ", latitude: " + this.latitude + " longtitude: " + this.longtitude);
        getSunParams(time);
        setSunParams();
    }

}
