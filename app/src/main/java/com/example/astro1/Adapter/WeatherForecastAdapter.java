package com.example.astro1.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.astro1.Common.Common;
import com.example.astro1.Model.WeatherForecastResult;
import com.example.astro1.R;
import com.squareup.picasso.Picasso;

public class WeatherForecastAdapter extends RecyclerView.Adapter<WeatherForecastAdapter.MyViewHolder> {

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_date_time;
        TextView txt_description;
        TextView txt_temperature;
        ImageView img_weather;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img_weather = itemView.findViewById(R.id.img_weather);
            txt_date_time = itemView.findViewById(R.id.txt_date_time);
            txt_temperature = itemView.findViewById(R.id.txt_temperature);
            txt_description = itemView.findViewById(R.id.txt_description);
        }
    }


    Context context;
    WeatherForecastResult result;

    public WeatherForecastAdapter(Context context, WeatherForecastResult result) {
        this.context = context;
        this.result = result;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_forecast_weather, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Picasso.get().load(new StringBuilder("https://openweathermap.org/img/w/")
                .append(result.list.get(i).weather.get(0).getIcon())
                .append(".png").toString())
                .resize(300, 300)
                .into(myViewHolder.img_weather);

        myViewHolder.txt_date_time.setText(new StringBuilder(Common.convertUnixToDate(result.list.get(i).dt)));
        myViewHolder.txt_description.setText(new StringBuilder(result.list.get(i).weather.get(0).getDescription()));
        myViewHolder.txt_temperature.setText(new StringBuilder(String.valueOf(result.list.get(i).main.getTemp()))
                .append(" °C"));

    }

    @Override
    public int getItemCount() {
        return result.list.size();
    }

}
