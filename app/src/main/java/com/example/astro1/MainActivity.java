package com.example.astro1;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    Spinner spinner;
    Button localizations;

    private final static String FRAG1 = "frag1";
    Moon moon = new Moon();
    Sun sun = new Sun();
    UpperFragment upperFragment;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.i("test", "onsave");
        if (upperFragment != null){
            String test = upperFragment.getTag();
            outState.putString(FRAG1, test);}
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        localizations=findViewById(R.id.localizations);
        localizations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddLocalization.class);
                startActivity(intent);
            }
        });
        setFragments(savedInstanceState);


    }

    private void setFragments(Bundle savedInstanceState) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (savedInstanceState != null) {
            String test = savedInstanceState.getString(FRAG1);
            upperFragment = (UpperFragment) getSupportFragmentManager().findFragmentByTag(test);
        } else {
            upperFragment = new UpperFragment();
        }
        fragmentManager.beginTransaction()
                .replace(R.id.upperRpw, upperFragment,"test")
                .commit();

        boolean isTablet = (getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

        //if not a tablet
        if (!isTablet) {
            LowerFragment lowerFragment = new LowerFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.lowerRow, lowerFragment, lowerFragment.getTag())
                    .commit();
        } else {
            //if tablet
            fragmentManager.beginTransaction()
                    .replace(R.id.sunPart, sun, sun.getTag())
                    .commit();
            fragmentManager.beginTransaction()
                    .replace(R.id.moonPart, moon, moon.getTag())
                    .commit();

        }
    }


}
