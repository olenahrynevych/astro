package com.example.astro1;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class UpperFragment extends Fragment {


    TextView timeText;
    Date currentTime;
    int selectedTime = 10000;

    Moon myMoon;
    Sun mySun;

    EditText latitudeStr, longtitudeStr;


    double latitude = 0.0, longtitude = 0.0;

    FragmentManager manager;
    private Spinner spinner;
    private Runnable runnable;

    Double savedFirst = 0.0;
    Double savedSecond = 0.0;

    List<Fragment> fragmentList;


    String[] minutes = {
            "5 sec",
            "10 sec",
            "15 min",
            "20 min"
    };

    final Handler handler = new Handler();
    Timer timer = new Timer(false);

    TimerTask timerTask;


    TimerTask timerTask2;

    public UpperFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myMoon = ((MainActivity) getActivity()).moon;
        mySun = ((MainActivity) getActivity()).sun;

        fragmentList = new ArrayList<>();
        fragmentList.add(myMoon);
        fragmentList.add(mySun);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View result = inflater.inflate(R.layout.fragment_settings, container, false);
        latitudeStr = result.findViewById(R.id.latitudeText);
        longtitudeStr = result.findViewById(R.id.longtitudeText);
        latitudeStr.setText(latitude + "");
        longtitudeStr.setText(longtitude + "");

        timeText = result.findViewById(R.id.time);

        //one min


        if (savedInstanceState != null) {
            savedFirst = savedInstanceState.getDouble("lat");
            savedSecond = savedInstanceState.getDouble("long");
            longtitudeStr.setText("test: "+ savedFirst);
        }



        timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        String time = getFormatedTime();
                        timeText.setText(time);
                        checkLocationChanged();
                    }
                });
            }
        };
        timerTask2 = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Date currentTime = Calendar.getInstance().getTime();
                        myMoon.updateTime(currentTime, latitude, longtitude);
                        mySun.updateTime(currentTime, latitude, longtitude);
                    }
                });
            }
        };
        timer.schedule(timerTask, 1000, 1000);
        timer.schedule(timerTask2, 1000, selectedTime);


        spinner = result.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, minutes);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getRefreshTime(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        return result;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    void getRefreshTime(int position) {

        //1 sec = 1000, 1 min = 60 sec = 60* 1 000,   5 min = 5 * 60 000;
        int oneSec = 1000;
        int oneMin = 60000;
        switch (position) {
            case 0:
                this.selectedTime = 5 * oneSec;
                break;
            case 1:
                this.selectedTime = 10 * oneSec;
                break;
            case 2:
                this.selectedTime = 15 * oneMin;
                break;
            case 3:
                this.selectedTime = 20 * oneMin;
                break;
            default:
                this.selectedTime = oneSec;
                break;

        }
        System.out.println("TIME IS UPDATED:  " + this.selectedTime);
    }

    private String getFormatedTime() {
        currentTime = Calendar.getInstance().getTime();
        String h;
        if (currentTime.getHours() < 10) {
            h = "0" + currentTime.getHours();
        } else {
            h = currentTime.getHours() + "";
        }

        String m;
        if (currentTime.getMinutes() < 10) {
            m = "0" + currentTime.getMinutes();
        } else {
            m = currentTime.getMinutes() + "";
        }

        String s;

        if (currentTime.getSeconds() < 10) {
            s = "0" + currentTime.getSeconds();
        } else {
            s = currentTime.getSeconds() + "";
        }

        return h + ":" + m + ":" + s;
    }

    private void checkLocationChanged() {

        if (savedFirst != 0.0) {
            latitude = savedFirst;
            latitudeStr.setText(latitude + "");
        }
        if (savedSecond != 0.0) {
            longtitude = savedSecond;
            longtitudeStr.setText(longtitude + "");
        }

        if (savedFirst == 0.0 && savedSecond == 0.0) {
            String firstArg = latitudeStr.getText().toString();
            String secondArg = longtitudeStr.getText().toString();
            if (!firstArg.equals("") && !secondArg.equals("")) {
                Double lat = Double.parseDouble(firstArg);
                Double log = Double.parseDouble(secondArg);
                if ((lat >= -90 && lat <= 90) && (log >= -180 && log <= 180)) {
                    longtitude = log;
                    latitude = lat;
                    System.out.println("Coordinates are updated!");
                    latitudeStr.setText(latitude + "");
                    longtitudeStr.setText(longtitude + "");
                } else {
                    Toast.makeText(getContext(), "Coordinates are not correct!", Toast.LENGTH_SHORT).show();
                    System.out.println("Coordinates are NOT updated!");

                }

            }


        }


    }


    @Override
    public void onStop() {
        super.onStop();
        timer.cancel();
        timer.purge();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putDouble("lat", Double.parseDouble(latitudeStr.getText().toString()));
        outState.putDouble("long", Double.parseDouble(longtitudeStr.getText().toString()));
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
