package com.example.astro1;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ForecastActivity extends AppCompatActivity {


    String userInput;

    ViewPager pager;
    PagerAdapter pagerAdapter;

    TodayFragment todayFragment = new TodayFragment();
    ForecastWeatherFragment forecastWeatherFragment = new ForecastWeatherFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);
        userInput = getIntent().getStringExtra("localizationInfo");
        Bundle bundle = new Bundle();
        bundle.putString("localization",userInput);

        pager = (ViewPager) findViewById(R.id.forecastContainer);
        pagerAdapter = new ListPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

    }


    class ListPagerAdapter extends PagerAdapter {

        FragmentManager fragmentManager;
        Fragment[] fragments;

        ListPagerAdapter(FragmentManager fm) {
            fragmentManager = fm;
            fragments = new Fragment[2];
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            assert (0 <= position && position < fragments.length);
            FragmentTransaction trans = fragmentManager.beginTransaction();
            trans.remove(fragments[position]);
            trans.commit();
            fragments[position] = null;
        }

        @Override
        public Fragment instantiateItem(ViewGroup container, int position) {
            Fragment fragment = getItem(position);
            FragmentTransaction trans = fragmentManager.beginTransaction();
            trans.add(container.getId(), fragment, "fragment:" + position);
            trans.commit();
            return fragment;
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        public Fragment getItem(int position) {
            assert (0 <= position && position < fragments.length);
            if (fragments[position] == null) {
                if (position == 0)
                    fragments[position] = (todayFragment = new TodayFragment());
                else if (position == 1)
                    fragments[position] = (forecastWeatherFragment = new ForecastWeatherFragment());

            }
            return fragments[position];
        }
    }
}
